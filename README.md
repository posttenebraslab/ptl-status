PTL-Status-API
=======================

Used for updating the Hackerspace Space API Json file and more.

**Please see https://www.posttenebraslab.ch/status for more information about the use at Post Tenebras Lab**

## What is the SpaceAPI?

Check the official site: http://spaceapi.net/

>What is the Space API?
>
>The purpose of the Space API is to define a unified specification across the hackerspaces that can be used to expose information to web apps or any other application. The specification is based on the JSON data interchange format.

## What is this project?

- A simple webapp/"API" to update the Space API Json file.
- Update can be done from a simple web page
- Or simply send a POST request from any device
- Can return different hackerspace logo depending on status
- Retrive info from Json using HTTP URL

**Please see https://www.posttenebraslab.ch/status for more information**

Note: This project is mainly intended for use at the PTL Hackerspace.
