import os
import time
from typing import Annotated, Any

from fastapi import FastAPI, Form, HTTPException
from fastapi import status as http_status
from fastapi.responses import FileResponse, Response
from fastapi.staticfiles import StaticFiles

API_KEY = os.getenv('PTL_STATUS_API_KEY')

app = FastAPI()
app.mount('/html', StaticFiles(directory='srv/html'))
app.mount('/img/static', StaticFiles(directory='srv/img'))


class State:
    open: bool = False
    message: str = ''
    lastchange: int = 0
    people_now_present: int = 0


STATE = State()


@app.get('/img/small_status.png')
def small_status() -> Response:
    filename = 'open_small.png' if STATE.open else 'closed_small.png'
    return FileResponse(f'srv/img/{filename}')


@app.get('/img/logo_status.png')
def logo_status() -> Response:
    filename = 'open_logo.png' if STATE.open else 'closed_logo.png'
    return FileResponse(f'srv/img/{filename}')


@app.get('/img/logo.png')
def logo() -> Response:
    filename = 'open_logo.png' if STATE.open else 'normal_logo.png'
    return FileResponse(f'srv/img/{filename}')


@app.get('/img/')
def img_info_page() -> Response:
    return FileResponse('srv/html/info_img.html')


@app.get('/img/ptl_control_panel.jpg')
def ptl_control_panel() -> Response:
    return FileResponse('srv/img/ptl_control_panel.jpg')


@app.get('/form_styles.css')
def form_styles() -> Response:
    return FileResponse('srv/html/form_styles.css')


@app.get('/change')
@app.get('/change_status')
def change_status() -> Response:
    return FileResponse('srv/html/change_status.html')


@app.post('/change')
@app.post('/change_status')
def change_status_post(
    api_key: Annotated[str, Form()],
    status: Annotated[str, Form()],
    open_closed: Annotated[str, Form()],
    people_now_present: Annotated[int, Form()],
) -> str:
    if api_key != API_KEY:
        raise HTTPException(http_status.HTTP_401_UNAUTHORIZED, 'Sorry, the API Key is invalid')

    STATE.open = open_closed == 'open'
    STATE.message = status
    STATE.people_now_present = people_now_present
    STATE.lastchange = int(time.time())
    return 'Information updated'


@app.get('/')
@app.get('/info')
@app.get('/info/')
def info_ptl_status_api() -> Response:
    return FileResponse('srv/html/info_ptl_status_api.html')


@app.get('/info/{tag}')
@app.get('/info/{tag}/')
def info(tag: str) -> str | dict[str, Any]:
    return str(get_status()[tag])


@app.get('/info/{tag}/{tag2}')
@app.get('/info/{tag}/{tag2}/')
def info2(tag: str, tag2: str) -> str | dict[str, Any]:
    return str(get_status()[tag][tag2])


@app.get('/info/{tag}/{tag2}/{tag3}')
@app.get('/info/{tag}/{tag2}/{tag3}/')
def info3(tag: str, tag2: str, tag3: str) -> str | dict[str, Any]:
    return str(get_status()[tag][tag2][tag3])


@app.get('/status.json')
@app.get('/json/')
@app.get('/json')
def return_json() -> dict[str, Any]:
    return get_status()


def get_status() -> dict[str, Any]:
    now = int(time.time())
    if STATE.lastchange < (now - 1800):
        STATE.open = False
        STATE.message = 'Lab status unknown: emmergency shutdown ! [No update from PTL control panel in last 30m]'
        STATE.people_now_present = 0
        STATE.lastchange = now

    return dict(
        api='0.13',
        space='Post Tenebras Lab',
        logo='https://www.posttenebraslab.ch/images/logo_ptl_m.png',
        url='https://www.posttenebraslab.ch',
        location=dict(
            address='Avenue de la Praille 36, 1227 Carouge, Switzerland',
            lat=46.187134981155396,
            lon=6.133659482002258,
        ),
        state=dict(
            icon=dict(
                closed='https://www.posttenebraslab.ch/images/ptl_closed.png',
                open='https://www.posttenebraslab.ch/images/ptl_open.png',
            ),
            lastchange=STATE.lastchange,
            message=STATE.message,
            open=STATE.open,
        ),
        contact=dict(
            email='comite@posttenebraslab.ch',
            facebook='https://www.facebook.com/PTL.CH',
            phone='+41 22 566 01 87',
            twitter='@posttenebraslab',
        ),
        issue_report_channels=[
            'email',
        ],
        sensors=dict(
            people_now_present=[
                dict(
                    value=STATE.people_now_present,
                ),
            ],
        ),
        feeds=dict(
            blog=dict(
                type='application/rss+xml',
                url='https://www.posttenebraslab.ch/wiki/feed.php?ns=blog&num=15',
            ),
            calendar=dict(
                type='text/calendar',
                url='https://cloud.posttenebraslab.ch/remote.php/dav/public-calendars/BpQK9rpG4kteYpMd?export',
            ),
            wiki=dict(
                type='application/rss+xml',
                url='https://www.posttenebraslab.ch/wiki/feed.php',
            ),
        ),
        cache=dict(
            schedule='m.02',
        ),
        projects=[
            'https://www.posttenebraslab.ch/wiki/projects/',
            'https://github.com/PostTenebrasLab',
            'https://gitlab.com/posttenebraslab',
        ],
    )
